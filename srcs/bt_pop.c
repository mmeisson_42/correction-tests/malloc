
#include <stdlib.h>
#include "test.h"

void	bt_pop(backtrace bt)
{
	size_t		i;

	if (bt)
	{
		pthread_mutex_lock(&bt->lock);
		for (i = 0; bt->backtrace[i][0] != '\0'; i++) { }

		if (i > 0) {
			bt->backtrace[i - 1][0] = 0;
		}
		pthread_mutex_unlock(&bt->lock);
	}
}
