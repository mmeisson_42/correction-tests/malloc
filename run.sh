#!/bin/sh

if [[ "$OSTYPE" == "linux-gnu"* || "$OSTYPE" == "freebsd"* ]]; then
    export LD_PRELOAD="../libft_malloc.so"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    # Mac OSX
    export DYLD_INSERT_LIBRARIES="../libft_malloc.so"
    export DYLD_FORCE_FLAT_NAMESPACE=1
else
    echo "you're on your own";
    exit(1)
fi
$@
